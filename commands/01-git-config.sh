basic config:
git config --global user.name "Dehya Qalbi"
git config --global user.email "example@gmail.com"

config for integration with vscode:
git config --global core.editor "code --wait"
git config --global diff.tool "default-difftool"
git config --global difftool.default-difftool.cmd "code --wait --diff \$LOCAL \$REMOTE"

check config:
git config --list --show-origin
