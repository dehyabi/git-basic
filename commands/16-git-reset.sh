git reset --soft hash: move HEAD to desired-hash and move all committed files after HEAD now to staging index 

git reset --mixed (default) hash: move HEAD to desired-hash and move all commited files after HEAD now to working directory

git reset --hard hash: move HEAD to desired-hash and delete all files in committed, staged, and working directory after HEAD now
