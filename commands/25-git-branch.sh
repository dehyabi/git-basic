git checkout branch-name : go to your desired branch

git checkout -b branch-name : create new branch on local repo

git push -u origin branch-name : push your local branch to remote repo

git branch : check list of local branches

git branch -r : check list of remote branches

